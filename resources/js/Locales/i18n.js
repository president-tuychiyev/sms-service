import { createI18n } from 'vue-i18n/dist/vue-i18n.cjs'
import uz from '@/Locales/uz.json'
import ru from '@/Locales/ru.json'
import en from '@/Locales/en.json'

const messages = { uz: uz, ru: ru, en: en }

const i18n = createI18n({
    locale: JSON.parse(localStorage.getItem('settings')).locale,
    legacy: false,
    ffallbackLocale: 'uz',
    messages: messages,
})

export default i18n
