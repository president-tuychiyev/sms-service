<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = [
            [
                'name' => "Javlonbek Tuychiyev",
                'email' => "president7566@gmail.com",
                'phone' => '+998905246866',
                'password' => Hash::make('9804')
            ]
        ];

        User::truncate();
        User::insert($users);
    }
}
