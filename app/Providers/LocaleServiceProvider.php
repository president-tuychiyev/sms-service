<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LocaleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->setRouteLang();
    }

    public function setRouteLang()
    {
        $locale = request()->segment(1);
        $language = '';

        if (isset(config('app.locales')[$locale])) {
            app()->setLocale($locale);
            $language = $locale;
        } else {
            app()->setLocale('ru');
            $language = 'ru';
        }

        config()->set('language', ucwords($language));
    }
}
